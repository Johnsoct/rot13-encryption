import os
import jinja2
import webapp2

# Variable to store template directory for jinja
template_dir = os.path.join(os.path.dirname(__file__), 'templates')

#instantiate Jinja Environment
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir), autoescape=True)

# Syntax shortcuts - Helper Function
class Handler(webapp2.RequestHandler):
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)


    def render_str(self, template, **params):
        """
        Function:
            'render_str' takes a filename (template) and a bunch of params (**params),
            loads the file (template) and create an jinja template from it.

            't.render(params)' renders our jinja template with the passed params.
        """
        t = jinja_env.get_template(template)
        return t.render(params)

    def render(self, template, **kw):
        """
        Function:
            'render' takes a template and a dictionary of params. 'render'
            actually sends the template back to the browser.
        Notes:
            **kw = dict of keys:attributes -> keys = arguments, attributes = values
            **kw is used when you need to handle named arguments that haven't been
            defined in advance
        """
        self.write(self.render_str(template, **kw))

class MainPage(Handler):

    def get(self):
        """
        Function:
            GET the template html and render it in the browser.

            self.render('', n=n) GETS the html template in our
            /file/templates/ directory and then passes parameters (**kw), which
            are used by Handler.render and Handler.render_str within .render.
        Notes:
            self.request.get('') GETS a value from the url, such as name or n.

            When GET gets a value from the url, it's a string, so to compare it
            to a integer in a statment syntax in your HTML, you have to convert
            it to an integer.
        """
        self.render('rot13.html')

    def post(self):
        # Variable represents user input in textarea (name='text')
        user_input = self.request.get('text')

        # Convert text
        converted_input = rot13conversion(user_input)

        # Return new text
        self.render('rot13.html', converted_input = converted_input)

def rot13conversion(user_input):
    """
    Function:
        If user_input exists, iterate over every index of input.
        If index is a character (letter), convert letter to int of index
            if int is closer than 13 to 26 (alpha.length),

    """
    if user_input:
        # Converted string
        x = ''
        for c in user_input:
            if c.isalpha():
                # unicode integer of current index
                i = ord(c)
                if i < 97:
                    if i + 13 <= 90:
                        x += chr(i + 13)
                    if i + 13 > 90:
                        x += chr(i - 13)
                if i > 96:
                    if i + 13 <= 122:
                        x += chr(i + 13)
                    if i + 13 > 122:
                        x += chr(i - 13)
            else:
                x += c
        return x
    else:
        user_input = ''


app = webapp2.WSGIApplication([('/', MainPage)], debug=True)
