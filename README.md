# Text to ROT13 | ROT 13 to Text

### Languages, Libraries, Frameworks
* python27
* HTML
* CSS
* Google App Engine

### Installation Instructions

### Project Requirements
To consider the project acceptable:
* Text Field:
  * Converts text to ROT13 text
  * Converts ROT13 text to text
  * WHILE:
    * PRESERVING:
      * Case sensitivity
      * Punctuation
      * White space
    * Escaping HTML
* Form:
  * Must be POST not GET

Project must be live via Google App Engine.

### Issues
Email is not currently checked:
  If email present:
    validate
    
    If email is valid:
      redirect
    else:
      return error
  else:
    redirect

### License
Licensed under the terms of the MIT license.
